/* -------------------------------------------------------------------------- */
/*                                   SERVER                                   */
/* -------------------------------------------------------------------------- */

/* --------------------------- REQUIRE STATEMENTS --------------------------- */

const express = require('express')
const exphbs = require('express-handlebars')
const mongoose = require('mongoose')
const userRouter = require('./routers/user_router')
const cookieSession = require('cookie-session')
const Keygrip = require('keygrip')

const app = express()
const port = process.env.PORT || 3000
const hbs = exphbs.create({
    helpers: {
        eq: function (value, comparator) {
            return value === comparator
        },
    },
})

/* ------------------------------ SET UP MONGO ------------------------------ */

let mongoUrl = 'mongodb://localhost:27017/cycloove'

if (process.env.NODE_ENV === 'production') {
    mongoUrl =
        'mongodb://Cycloove:gytky8-Vajkam-gaqgim@lon5-c12-2.mongo.objectrocket.com:43017,lon5-c12-1.mongo.objectrocket.com:43017,lon5-c12-0.mongo.objectrocket.com:43017/Cycloove-app?replicaSet=bab6cbd6d8d0469e9f2c182a842ba698&retryWrites=false'
}

mongoose.set('useFindAndModify', false)
mongoose
    .connect(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => console.log('connection succeeded'))
    .catch((error) => console.log('err', error))

/* ----------------------------- SET UP EXPRESS ----------------------------- */

app.engine('handlebars', hbs.engine)
app.set('view engine', 'handlebars')
app.set('trust proxy', 1) // trust first proxy

app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(express.static(`${__dirname}/public`))

app.use(
    cookieSession({
        name: 'session',
        keys: new Keygrip(['key1', 'key2'], 'SHA384', 'base64'),

        // Cookie Options
        maxAge: 24 * 60 * 60 * 1000, // 24 hours
    })
)

/* ------------------------------- MIDDLEWARE ------------------------------- */

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Credentials', 'true')

    if (!req.session) {
        req.session = null
    }

    res.locals.is_logged = req.session.user
    return next()
})

/* --------------------------------- ROUTES --------------------------------- */

app.use('/user', userRouter)

app.get('/', function (req, res) {
    return res.render('home')
})

app.get('/login', function (req, res) {
    return res.render('login')
})

/* --------------------------------- SERVER --------------------------------- */

app.listen(port, () => console.log(`app listening at http://localhost:${port}`))
