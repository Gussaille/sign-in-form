#**_DOCUMENTATION_**

## Server.js

La configuration de l'application se trouve dans ce fichier. On y retrouve la déclaration du `middleware` utilisé pour les routes ainsi que la méthode permettant de lancer le serveur. Il se situe à la racine du projet et est le fichier à executer pour lancer l'application.

## Models

Ce dossier contient le fichier de configuration du model utiliser pour le schema d'un objet `User` qui sera stocké en base de données.

## Routers

Ce dossier contient un fichier de route qui possède toutes les routes en lien avec la gestion d'un utilisateur. Nous avons choisi de créer un fichier à part pour ces routes afin de ne pas surcharger le fichier `server.js`.

## Public

Ce dossier est composé de tous les élèments static du site. Il s'agit des fichiers de style et des fichiers javascripts utilisés uniquement côté client. Nous y retrouvons aussi les images, le fichier contenant les données de la carte, ainsi que notre `service worker`.

## Views

Ce dossier contient les `views` correspondant aux différentes page de l'application. Ainsi que 2 sous-dossiers:

-   **layouts**
    Qui contient le fichier dont les `views`du projet vont hériter.

-   **partials**
    Qui contient les fichiers qui sont retrouvable sur toutes les pages du site, tels que le `header`et le `footer`.
