if (document.querySelector('#map')) {
  var map = L.map('map').setView([48.82008509803184, 2.2375005805060457], 11)

  var markers
  var cyclooveIcon = L.icon({
    iconUrl: './images/cycloove.png',
    iconSize: [40, 40], // size of the icon
    iconAnchor: [12, 40], // point of the icon which will correspond to marker's location
    popupAnchor: [7, -20], // point from which the popup should open relative to the iconAnchor
  })

  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    minZoom: 5,
    maxZoom: 20,
    tileSize: 512,
    zoomOffset: -1,
  }).addTo(map)

  function handlePermission() {
    /* navigator.permissions.query({ name: 'geolocation' }).then(function (result) {
      if (result.state == 'granted') {
        report(result.state)
        geoBtn.style.display = 'none'
      } else if (result.state == 'prompt') {
        report(result.state)
        geoBtn.style.display = 'none'
        navigator.geolocation.getCurrentPosition(revealPosition, positionDenied, geoSettings)
      } else if (result.state == 'denied') {
        report(result.state)
        geoBtn.style.display = 'inline'
      }
      result.onchange = function () {
        report(result.state)
      }
    }) */

    navigator.geolocation.getCurrentPosition((position) =>
      map.setView([position.coords.latitude, position.coords.longitude])
    )
  }

  function report(state) {
    console.log('Permission ' + state)
  }

  handlePermission()

  function clickZoom(e) {
    map.setView(e.target.getLatLng(), 15)
  }

  function getParkingsLocations() {
    fetch('/data.json')
      .then((response) => {
        return response
          .json()
          .then((data) => {
            markers = data.map((marker) => {
              return L.marker([marker.latitude, marker.longitude], {
                icon: cyclooveIcon,
              })
                .bindPopup(
                  `<div class="marker-informations">
                    <p><span class="marker--location">Places disponibles : ${marker.available_location}/${marker.location}</span>
                    <span class='marker--name'>${marker.name}</span>,
                    <br/> ${marker.address}, ${marker.town}</p>
                    <a href="/user/booking/${marker.id}" class="btn-book">Réserver</a>
                  </div>`
                )
                .addTo(map)
            })

            return data
          })
          .then((data) => {
            let places
            let availableArray = []

            data.map((parking) => {
              places = parking.available_location
              availableArray.push(places)
            })

            let spaces = data.length * 50
            let availableSpaces = availableArray.reduce((a, b) => {
              return a + b
            }, 0)

            document.querySelector('.availableSpaces').innerHTML =
              availableSpaces
            document.querySelector('.allSpaces').innerHTML = spaces
          })
          .catch((err) => {
            console.log(err)
          })
      })
      .catch((err) => console.log(err))
  }

  getParkingsLocations()

  //User Location
  function onLocationFound(e) {
    L.marker(e.latlng)
      .addTo(map)
      .bindPopup('Vous êtes ici')
      .openPopup()
      .on('click', clickZoom)
  }

  function onLocationError(e) {
    alert(e.message)
  }

  map.on('locationfound', onLocationFound)
  map.on('locationerror', onLocationError)

  map.locate({ setView: true, maxZoom: 16 })
}

if ('serviceWorker' in navigator) {
  navigator.serviceWorker
    .register('/sw.js')
    .then((reg) => console.log('service worker registered', reg))
    .catch((err) => console.log('service worker not registered', err))
}
