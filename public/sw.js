const staticCacheName = 'site-static-v0'
const assets = [
  '/app.js',
  '/app.css',
  '/style.css',
  '/data.json',
  '/images/cycloove.png',
  'https: //unpkg.com/leaflet@1.7.1/dist/leaflet.css',
  'https://unpkg.com/leaflet@1.7.1/dist/leaflet.js',
]

self.addEventListener('install', (evt) => {
  // console.log('installation');
  self.skipWaiting()

  evt.waitUntil(
    caches.open(staticCacheName).then((cache) => {
      // console.log('caching shell assets')
      cache.addAll(assets)
      // console.log(cache);
    })
  )
}) // activate event

self.addEventListener('activate', (evt) => {
  // console.log('activation')

  evt.waitUntil(
    caches.keys().then((keys) => {
      return Promise.all(keys.filter((key) => key !== staticCacheName).map((key) => caches.delete(key)))
    })
  )
})

// fetch event
self.addEventListener('fetch', (e) => {
  e.respondWith(
    caches.match(e.request).then((r) => {
      // console.log('[Service Worker] Récupération de la ressource: '+e.request.url);
      return (
        r ||
        fetch(e.request).then((response) => {
          return caches.open(staticCacheName).then((cache) => {
            // console.log('[Service Worker] Mise en cache de la nouvelle ressource: '+e.request.url);
            cache.put(e.request, response.clone())
            return response
          })
        })
      )
    })
  )
})

const dynamicCacheName = 'site-dynamic-v1.1' // activate event
self.addEventListener('activate', (evt) => {
  evt.waitUntil(
    caches.keys().then((keys) => {
      return Promise.all(keys.filter((key) => key !== dynamicCacheName).map((key) => caches.delete(key)))
    })
  )
}) // fetch event

self.addEventListener('fetch', (evt) => {
  evt.respondWith(
    caches.match(evt.request).then((cacheRes) => {
      return (
        cacheRes ||
        fetch(evt.request).then((fetchRes) => {
          return caches.open(dynamicCacheName).then((cache) => {
            cache.put(evt.request.url, fetchRes.clone())
            return fetchRes
          })
        })
      )
    })
  )
})
