/* -------------------------------------------------------------------------- */
/*                                 USER ROUTER                                */
/* -------------------------------------------------------------------------- */

/* --------------------------- REQUIRE STATEMENTS --------------------------- */

const express = require('express')
const bcrypt = require('bcrypt')
const UserModel = require('./../models/user_model')
const data = require('../public/data.json')

/* ----------------------------- SET UP EXPRESS ----------------------------- */

const router = express.Router()
const { check, validationResult } = require('express-validator')

const saltRounds = 10
const registerValidate = [
    check('firstname', 'Le champ prénom est invalide.').notEmpty(),
    check('lastname', 'Le champ nom est invalide.').notEmpty(),
    check('email', 'Le champs email est invalide.').isEmail().trim().escape().normalizeEmail(),
    check('password')
        .isLength({ min: 8 })
        .withMessage('Le mot de passe doit contenir au moins 8 caractères.')
        .matches('[0-9]')
        .withMessage('Le mot de passe doit contenir un chiffre.')
        .matches('[A-Z]')
        .withMessage('Le mot de passe doit contenir une majuscule.')
        .trim()
        .escape(),
]

const loginValidate = [
    check('email', 'Le champs email est invalide.').isEmail().trim().escape().normalizeEmail(),
    check('password')
        .isLength({ min: 8 })
        .withMessage('Le mot de passe doit contenir au moins 8 caractères.')
        .matches('[0-9]')
        .withMessage('Le mot de passe doit contenir un chiffre.')
        .matches('[A-Z]')
        .withMessage('Le mot de passe doit contenir une majuscule.')
        .trim()
        .escape(),
]

/* ------------------------------- GET ROUTES ------------------------------- */

router.get('/logout', (req, res) => {
    req.session = null
    return res.redirect('/')
})

router.get('/account', (req, res) => {
    if (!req.session.user) {
        return res.redirect('/login')
    } else {
        UserModel.findOne({ email: req.session.user.email })
            .then((_user) => {
                if (_user) {
                    res.render('account', {
                        user: {
                            firstname: _user.firstname,
                            lastname: _user.lastname,
                            email: _user.email,
                            reservations: _user.reservations.map((reservation) => {
                                return {
                                    location: reservation.location,
                                    reservation: reservation.reservation,
                                    name: reservation.name,
                                    address: reservation.address,
                                }
                            }),
                        },
                    })
                } else {
                    res.status(500).send(`Utilisateur avec l\'email ${req.session.user.email} est introuvable`)
                }
            })
            .catch((err) => console.log(err))
    }
})

router.get('/booking/:id', (req, res) => {
    if (!req.session.user) {
        return res.redirect('/login')
    }

    const [location] = data.filter((marker) => marker.id === (req.params.id ? parseInt(req.params.id) : 1))

    return res.render('booking', {
        location_id: req.params.id ? req.params.id : 1,
        location,
    })
})

/* ------------------------------- POST ROUTES ------------------------------ */

router.post('/register', registerValidate, (req, res) => {
    const errors_register = validationResult(req)

    if (!errors_register.isEmpty()) {
        return res.status(400).render('login', {
            errors_register: errors_register.array(),
        })
    } else {
        let firstname = req.body.firstname
        let lastname = req.body.lastname
        let email = req.body.email
        let pass = req.body.password

        bcrypt
            .hash(pass, saltRounds)
            .then((hash) => {
                if (hash) {
                    var data = {
                        firstname: firstname,
                        lastname: lastname,
                        email: email,
                        password: hash,
                    }

                    UserModel.exists({ email })
                        .then((exists) => {
                            if (exists) {
                                res.send(`L\'utilisateur avec l\'email "${email}" existe déjà !`)
                            } else {
                                const newUserModel = new UserModel(data)

                                newUserModel.save().then((user) => {
                                    if (user) {
                                        req.session.user = {
                                            email: user.email,
                                        }

                                        return res.redirect('/user/account')
                                    } else {
                                        return res.render('home', {
                                            errors_register: [
                                                {
                                                    msg: 'Une erreur est survenue',
                                                },
                                            ],
                                        })
                                    }
                                })
                            }
                        })
                        .catch((err) => console.log(err))
                } else {
                    return res.render('home', {
                        errors_register: [
                            {
                                msg: 'Une erreur est survenue',
                            },
                        ],
                    })
                }
            })
            .catch((err) => console.log(err))
    }
})

router.post('/login', loginValidate, (req, res) => {
    const errors_login = validationResult(req)

    if (!errors_login.isEmpty()) {
        return res.status(400).render('login', {
            errors_login: errors_login.array(),
        })
    } else {
        let email = req.body.email
        let pass = req.body.password

        UserModel.exists({ email })
            .then((exists) => {
                if (exists) {
                    UserModel.findOne({ email })
                        .then((user) => {
                            if (user) {
                                bcrypt
                                    .compare(pass, user.password)
                                    .then((success) => {
                                        if (success) {
                                            req.session.user = {
                                                email: user.email,
                                            }

                                            return res.redirect('/user/account')
                                        } else {
                                            return res.render('login', {
                                                errors_login: [
                                                    {
                                                        msg: 'Mot de passe incorrect, veuillez réessayer',
                                                    },
                                                ],
                                            })
                                        }
                                    })
                                    .catch((err) => console.log(err))
                            } else {
                                return res.render('login', {
                                    errors_login: [
                                        {
                                            msg: 'Aucun utilisateur trouvé avec cette email',
                                        },
                                    ],
                                })
                            }
                        })
                        .catch((err) => console.log(err))
                } else {
                    return res.render('login', {
                        errors_login: [
                            {
                                msg: `Aucun utilisateur avec cette email "${email}" n'existe !`,
                            },
                        ],
                    })
                }
            })
            .catch((err) => console.log(err))
    }
})

router.post('/book', (req, res) => {
    const formData = req.body

    if (!req.session.user) {
        return res.redirect('/login')
    }

    const email = req.session.user.email

    if (formData) {
        UserModel.findOne({ email })
            .then((user) => {
                if (user) {
                    const [location] = data.filter((marker) => marker.id === parseInt(formData.location_id))

                    const newReservation = {
                        location: {
                            latitude: location.latitude,
                            longitude: location.longitude,
                        },
                        reservation: {
                            date: formData.reservation_date,
                            begin: formData.reservation_begin,
                            end: formData.reservation_end,
                        },
                        name: location.name,
                        address: location.address,
                    }

                    UserModel.findOneAndUpdate(
                        { _id: user._id },
                        {
                            $push: {
                                reservations: newReservation,
                            },
                        }
                    )
                        .then((user) => {
                            if (user) {
                                return res.redirect('/user/account')
                            } else {
                                return res.status(500).send("Une erreur est survenue lors de l'ajout d'une réservation")
                            }
                        })
                        .catch((err) => console.log(err))
                } else {
                    return res.status(500).send('Utilisateur introuvable')
                }
            })
            .catch((err) => console.log(err))
    } else {
        return res.status('500').send('Une erreur est survenue lors du traitement du formulaire')
    }
})

module.exports = router
