/* -------------------------------------------------------------------------- */
/*                                 USER MODEL                                 */
/* -------------------------------------------------------------------------- */

/* --------------------------- REQUIRE STATEMENTS --------------------------- */

const mongoose = require('mongoose')
const Schema = mongoose.Schema

/* --------------------------------- SCHEMA --------------------------------- */

const UserSchema = new Schema({
    firstname: String,
    lastname: String,
    email: String,
    password: String,
    reservations: [
        {
            location: {
                latitude: Number,
                longitude: Number,
            },
            reservation: {
                date: String,
                begin: String,
                end: String,
            },
            name: String,
            address: String,
        },
    ],
})

/* ---------------------------------- MODEL --------------------------------- */

const UserModel = mongoose.model('User', UserSchema, 'users')

module.exports = UserModel
